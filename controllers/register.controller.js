﻿var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');

router.get('/', function (req, res) {
    res.render('register');
});

router.post('/', function (req, res) {
    // register using api to maintain clean separation between layers
    request.post({
        url: config.apiUrl + '/users/register',
        form: req.body,
        json: true
    }, function (error, response, body) {
        if (error) {
            return res.render('register', { error: 'An error occurred' });
        }

        if (response.statusCode !== 200) {
            return res.render('register', {
                error: response.body,
                name: req.body.name,
                matricNo: req.body.matricNo,
                username: req.body.username,
                faculty: req.body.faculty,
                email: req.body.email,
                phoneNo: req.body.phoneNo,
                ic: req.body.ic,
                nation: req.body.nation,
                gender: req.body.gender,
                religion: req.body.religion,
                marriage: req.body.marriage,
                dob: req.body.dob,
                sob: req.body.sob,
                race: req.body.race,
                address: req.body.address,
                yearEnroll: req.body.yearEnroll,
                yearGrad: req.body.yearGrad,
                status: req.body.status,
                dissability: req.body.dissability,
                course: req.body.course

            });
        }

        // return to login page with success message
        req.session.success = 'Registration successful';
        return res.redirect('/login');
    });
});

module.exports = router;